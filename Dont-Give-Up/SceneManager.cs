﻿//AUTHOR Christopher Grimm
//LAST EDITED: 28.02.2016


using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dont_Give_Up
{
    //SCENE MANAGER TO CONTROLL THE CURRENT GAME SCENE
    static class SceneManager
    {
        public static Scene CurrentScene { get; private set; }
        public static bool IsPaused { get; set; }

        public static T LoadScene<T>() where T : Scene, new()
        {
            if(CurrentScene != null )
            {
                CurrentScene.UnloadContent();
            }
            IsPaused = false;
            CurrentScene = new T();
            CurrentScene.LoadContent();
            return ( T ) CurrentScene;
        }

        public static void UpdateScene(GameTime gameTime)
        {
            CurrentScene.Update(gameTime);
        }

        public static void DrawScene(SpriteBatch spriteBatch)
        {
            CurrentScene.Draw(spriteBatch);
        }
    }
}
