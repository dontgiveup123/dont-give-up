﻿//AUTHOR Christopher Grimm
//LAST EDITED: 28.02.2016

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dont_Give_Up
{
    //CREATE THE MAIN MENU
    class MainMenuDialog
    {
        public string Titel;
        public string Text;

        private UIImage background;
        private UILabel titel;
        private UILabel text;
        private UIButton nextButtonPlay;
        private UIButton nextButtonCredits;
        private UIButton nextButtonExit;
        private SpriteFont spriteFont;
        private UILabel buttonLabelPlay;
        private UILabel buttonLabelCredits;
        private UILabel buttonLabelExit;
        private SoundEffectInstance backgroundMusic;

        public void Show()
        {
            SpriteFont font = GameManager.LoadSpriteFont( "Aon Cari Celtic" );
            backgroundMusic = GameManager.LoadSoundEffect( "backgroundSound" ).CreateInstance();
            backgroundMusic.IsLooped = true;
            backgroundMusic.Play();

            background = new UIImage();
            background.Image = GameManager.Content.Load<Texture2D>( "landscape" );
            background.Position = new Vector2( 0, 0 );

            titel = new UILabel();
            titel.Font = font;
            titel.Position = new Vector2(600, 0);
            titel.Scale = 2f;
            titel.Text = Titel;

            text = new UILabel();
            text.Font = font;
            text.Position = new Vector2(600,45);
            text.Scale = 1f;
            text.Color = Color.Black;
            text.Text = Text;


            nextButtonPlay = new UIButton(GameManager.Content.Load<Texture2D>("menuButton"));
            nextButtonPlay.Position = new Vector2(425, 200);
            nextButtonPlay.OnPress += gameStart;

            nextButtonCredits = new UIButton( GameManager.Content.Load<Texture2D>( "menuButton" ) );
            nextButtonCredits.Position = new Vector2( 425, 260 );
            nextButtonCredits.OnPress += showCredits;


            nextButtonExit = new UIButton( GameManager.Content.Load<Texture2D>( "menuButton" ) );
            nextButtonExit.Position = new Vector2( 425, 320 );
            nextButtonExit.OnPress += gameExit;

            buttonLabelPlay = new UILabel();
            buttonLabelPlay.Font = font;
            buttonLabelPlay.Position = new Vector2( 665, 195 );
            buttonLabelPlay.Scale = 1.5f;
            buttonLabelPlay.Color = Color.White;
            buttonLabelPlay.Text = "Play";

            buttonLabelCredits = new UILabel();
            buttonLabelCredits.Font = font;
            buttonLabelCredits.Position = new Vector2( 665, 255 );
            buttonLabelCredits.Scale = 1.5f;
            buttonLabelCredits.Color = Color.White;
            buttonLabelCredits.Text = "Credits";

            buttonLabelExit = new UILabel();
            buttonLabelExit.Font = font;
            buttonLabelExit.Position = new Vector2( 665, 315 );
            buttonLabelExit.Scale = 1.5f;
            buttonLabelExit.Color = Color.White;
            buttonLabelExit.Text = "Exit";
        }

        private void showCredits()
        {
            var creditsScene = SceneManager.LoadScene<ShowCredits>();
            creditsScene.Titel = "Credits";
            creditsScene.Text = "Hi folks here are the amazing Credits to be ready from you!";
            creditsScene.UpdateText();
        }

        private void gameStart()
        {
            SceneManager.LoadScene<GameScene>();
        }
        
        private void gameExit()
        {
            Environment.Exit( 0 );
        }

        public void UnloadContent()
        {
            backgroundMusic.Stop();
            background.UnloadContent();
            titel.UnloadContent();
            text.UnloadContent();
            nextButtonPlay.UnloadContent();
            nextButtonCredits.UnloadContent();
            nextButtonExit.UnloadContent();
            buttonLabelPlay.UnloadContent();
            buttonLabelCredits.UnloadContent();
            buttonLabelExit.UnloadContent();

        }
    }
}
