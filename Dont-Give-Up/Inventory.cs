﻿//AUTHOR Christopher Grimm
//LAST EDITED: 28.02.2016

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dont_Give_Up
{
    enum ItemTypes
    {
        wood,
        stone
    }
    //INVENTORY AND GUI CLASS FOR GRAPHICAL USER-INTERFACE
    class Inventory : GameObject
    {

        //___________________GUI___________________
        private static int positionX = ( Map.Width * 32 + offset );
        private static int positionY = 245;
        private static int itemSpaceOffset = 25;
        private static int itemWordIdent = 15;
        private static int itemSpaceCountOffset = 80;
        private static int offset = 5;
        private Texture2D inventoryGui;
        private Texture2D infoPanelBuildingMode;
        private Texture2D infoPanelBuildingMaterial;


        //______________________Iventory________________
        private static int wood = 0;
        private static int stone = 0;
        private ItemTypes item;
        
        //ADD ITEM TO THE INVENTORY
        public static void AddItem(ItemTypes item, int count )
        {
            if(ItemTypes.wood == item )
            {
                wood += count;
            }
            if(ItemTypes.stone == item )
            {
                stone += count;
            }
        }
        
        //REMOVE ITEM FROM INVENTORY
        public static bool RemoveItem(ItemTypes item, int count )
        {
            if(ItemTypes.wood == item)
            {
                if( ( wood - count ) < 0 )
                {
                    wood = 0;
                    return false;
                }
                else
                {
                    wood -= 1;
                    return true;
                }
            }

            if( ItemTypes.stone == item )
            {
                if( ( stone - count ) < 0 )
                {
                    stone = 0;
                    return false;
                }
                else
                {
                    stone -= 1;
                    return true;
                }
            }
            return false;
        }

        //DRAW THE INVENTORY AND SHOWS THE GRAPHICAL INFORMATION
        public override void Draw(SpriteBatch spriteBatch)
        {
            SpriteFont font = GameManager.LoadSpriteFont( "Aon Cari Celtic" );
            inventoryGui = GameManager.LoadTexture2D( "inventory" );
            spriteBatch.Draw( inventoryGui, new Rectangle( positionX, 0, 215, 766 ), Color.White );

            infoPanelBuildingMode = GameManager.LoadTexture2D( "buttonStock1" );
            spriteBatch.Draw( infoPanelBuildingMode, new Rectangle( positionX, -10, 200, 55), Color.White );

            infoPanelBuildingMaterial = GameManager.LoadTexture2D( "buttonStock1" );
            spriteBatch.Draw( infoPanelBuildingMode, new Rectangle( positionX + 38, 185, 150, 55 ), Color.White );

            if(Player.BuildMode != true)
                spriteBatch.DrawString( font, "Collect Mode", new Vector2( positionX + ( itemWordIdent + 10 ), 4 ), Color.White );
            else
                spriteBatch.DrawString( font, "Build Mode", new Vector2(positionX + ( itemWordIdent + 10 ), 4), Color.White );

            if( Player.BuildMaterial != false )
                spriteBatch.DrawString( font, "Wood", new Vector2( positionX + ( itemWordIdent + 65 ), 197 ), Color.White );
            else
                spriteBatch.DrawString( font, "Stone", new Vector2( positionX + ( itemWordIdent + 65 ), 197 ), Color.White );


            spriteBatch.DrawString( font, "Wood: ", new Vector2( positionX + itemWordIdent, positionY ), Color.White );
            spriteBatch.DrawString( font, wood.ToString(), new Vector2( positionX + itemWordIdent + itemSpaceCountOffset, positionY ), Color.White );
            spriteBatch.DrawString( font, "Stone: ", new Vector2( positionX + itemWordIdent, positionY + itemSpaceOffset ), Color.White );
            spriteBatch.DrawString( font, stone.ToString(), new Vector2( positionX + itemWordIdent + itemSpaceCountOffset, positionY + itemSpaceOffset ), Color.White );
        }
    }
}
