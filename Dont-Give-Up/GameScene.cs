﻿//AUTHOR Christopher Grimm
//LAST EDITED: 28.02.2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace Dont_Give_Up
{
    class GameScene : Scene
    {
        private SoundEffectInstance backgroundMusic;

        public override void Draw( SpriteBatch spriteBatch )
        {
            throw new NotImplementedException();
        }

        public override void LoadContent()
        {
            //INIT MUSIC DURING GAMEPLAY
            backgroundMusic = GameManager.LoadSoundEffect( "backgroundSound" ).CreateInstance();
            backgroundMusic.IsLooped = true;
            backgroundMusic.Play();

            //INIT AND LOAD GAME
            GameManager.AddGameObject( new Inventory() );
            GameManager.AddMap( new Map( 0 ) );
            GameManager.LoadMap( 0 );
            GameManager.AddGameObject( new Player( new Vector2( 3, 12 ) ) );
        }

        //UNLOAD CONTENT WHEN SCENE IS CLOSED
        public override void UnloadContent()
        {
            backgroundMusic.Stop();
        }

        public override void Update( GameTime gameTime )
        {
        }
    }
}
