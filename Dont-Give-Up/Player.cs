﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;


namespace Dont_Give_Up
{
    //Author: Henrik Winter

    //the Player Class controll the movement of the Player gameobjekt and his interations with the map.
    class Player : GameObject
    {
        public static bool BuildMode;
        public static bool BuildMaterial = false;

        private bool active = true;
        private int reStapCount = 10;

        private Texture2D sprite;
        private Vector2 position;
        private int counterW = 0;
        private int counterA = 0;
        private int counterS = 0;
        private int counterD = 0;

        private Vector2 directionRight = new Vector2(1, 0);
        private Vector2 directionDown = new Vector2(0, 1);

        private Map map;

        public Player(Vector2 startPosition)
        {
            this.Name = "Player";
            this.sprite = GameManager.Content.Load<Texture2D>("player");
            this.position = startPosition;
            map = (Map)GameManager.FindGameObject("Map");
            InputManager.OnKeyPressed += OnKeyPressed;
            InputManager.OnKeyDown += OnKeyDown;
            InputManager.OnKeyReleased += OnKeyReleased;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sprite, position * sprite.Width, Color.White);
        }

        private void OnKeyPressed(Keys key)
        {
            if(active)
            {
                if (key == Keys.D)
                    Move(directionRight);

                if (key == Keys.A)
                    Move(-directionRight);

                if (key == Keys.W)
                    Move(-directionDown);

                if (key == Keys.S)
                    Move(directionDown);

                if (key == Keys.B)
                {
                    if (BuildMaterial)
                        BuildMaterial = false;
                    else
                        BuildMaterial = true;
                }

                if (key == Keys.F)
                {
                    if (BuildMode)
                        BuildMode = false;
                    else
                        BuildMode = true;
                }
                else
                    InteractionControl(key);
            }
        }

        private void OnKeyDown(Keys key)
        {
            if (active)
            {
                if (key == Keys.W)
                {
                    counterW++;
                    if (counterW == reStapCount)
                    {
                        Move(-directionDown);
                        counterW = 0;
                    }
                }

                if (key == Keys.A)
                {
                    counterA++;
                    if (counterA == reStapCount)
                    {
                        Move(-directionRight);
                        counterA = 0;
                    }
                }

                if (key == Keys.S)
                {
                    counterS++;
                    if (counterS == reStapCount)
                    {
                        Move(directionDown);
                        counterS = 0;
                    }
                }

                if (key == Keys.D)
                {
                    counterD++;
                    if (counterD == reStapCount)
                    {
                        Move(directionRight);
                        counterD = 0;
                    }
                }
            }
        }

        private void OnKeyReleased(Keys key)
        {
            if (key == Keys.W)
                counterW = 0;
            if (key == Keys.A)
                counterA = 0;
            if (key == Keys.S)
                counterS = 0;
            if (key == Keys.D)
                counterD = 0;
        }

        private void Move(Vector2 direction)
        {
            Vector2 targetPosition = position + direction;

            if (targetPosition.X==-1)
            {
                GameManager.UnloadMap();
                GameManager.LoadMap(-1);
                GameManager.AddGameObject(new Player(new Vector2(Map.Width - 1, 12)));
                active = false;
            }
            else if (targetPosition.X == Map.Width)
            {
                GameManager.UnloadMap();
                GameManager.LoadMap(1);
                GameManager.AddGameObject(new Player(new Vector2(0, 12)));
                active = false;
            }
            else
            {
                Tile targetTile = map.GetTile(targetPosition);
                if (targetTile.IsPassable)
                    position = targetPosition;
            }
        }

        private void InteractionControl(Keys key)
        {
            if(key == Keys.Right)
            {
                if (BuildMode)
                    map.PlaceWall((int)position.X + 1, (int)position.Y);
                else
                    if (map.GetTile((int)position.X + 1, (int)position.Y).Degradable)
                        map.Degrad((int)position.X + 1, (int)position.Y);
            }
               
            if(key == Keys.Up)
            {
                if (BuildMode)
                    map.PlaceWall((int)position.X, (int)position.Y - 1);
                else
                    if (map.GetTile((int)position.X, (int)position.Y - 1).Degradable)
                        map.Degrad((int)position.X, (int)position.Y - 1);
            }
                
            if(key == Keys.Down)
            {
                if (BuildMode)
                    map.PlaceWall((int)position.X, (int)position.Y + 1);
                else
                    if (map.GetTile((int)position.X, (int)position.Y + 1).Degradable)
                        map.Degrad((int)position.X, (int)position.Y + 1);
            }

            if(key == Keys.Left)
            {
                if (BuildMode)
                    map.PlaceWall((int)position.X - 1, (int)position.Y);
                else
                    if (map.GetTile((int)position.X - 1, (int)position.Y).Degradable)
                        map.Degrad((int)position.X - 1, (int)position.Y);
            }
        }

    }
}
