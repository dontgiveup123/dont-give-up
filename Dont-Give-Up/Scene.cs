﻿//AUTHOR Christopher Grimm
//LAST EDITED: 28.02.2016


using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Dont_Give_Up
{
    abstract class Scene
    {
        //METHODS TO CONTROLL THE SCENE
        public abstract void LoadContent();
        public abstract void Update(GameTime gameTime);
        public abstract void Draw(SpriteBatch spriteBatch);
        public abstract void UnloadContent();
    }
}
