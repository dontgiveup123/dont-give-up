﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Dont_Give_Up
{
    //Author: Henrik Winter, Chritopher Grimm

    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont spriteFont;

        public Game1()
        {
            graphics = new GraphicsDeviceManager( this );

            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            base.Initialize();
            graphics.IsFullScreen = true;
            graphics.ApplyChanges();
            IsMouseVisible = true;
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch( GraphicsDevice );

            GameManager.Content = this.Content;
            GameManager.SpriteBatch = this.spriteBatch;
            GameManager.SpriteFont = this.spriteFont;
            SceneManager.LoadScene<MainMenuScene>();
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update( GameTime gameTime )
        {
            if( GamePad.GetState( PlayerIndex.One ).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown( Keys.Escape ) )
                Exit();

            InputManager.Update();
            GameManager.Update( gameTime );
            SceneManager.UpdateScene( gameTime );
            UIManager.Update( gameTime );

            base.Update( gameTime );
        }

        protected override void Draw( GameTime gameTime )
        {
            GraphicsDevice.Clear( Color.Black );
            spriteBatch.Begin();
			
            UIManager.Draw( spriteBatch );
            spriteBatch.End();
            GameManager.Draw(spriteBatch);
            base.Draw( gameTime );
        }
    }
}
