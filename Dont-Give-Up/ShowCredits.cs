﻿//AUTHOR Christopher Grimm
//LAST EDITED: 28.02.2016


using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using System.Diagnostics;

namespace Dont_Give_Up
{
    //SHOWS THE CREDITS
    class ShowCredits : Scene
    {
        public string Titel;
        public string Text;

        private UIImage background;
        private UILabel titel;
        private UILabel text;
        private UILabel creditsInfo;
        private UIButton nextButtonExit;
        private SpriteFont spriteFont;
        private UILabel buttonLabelExit;
        private SoundEffectInstance backgroundMusic;

        private void BackToMainMenu()
        {
            SceneManager.LoadScene<MainMenuScene>();
        }
        
        //CREDITS INFORMATION
        public void UpdateText()
        {
            titel.Text = Titel;
            text.Text = Text;
            creditsInfo.Text = "\n Programmers: \n Henrik Winter \n Christopher Grimm \n\n Special Thanks to SAE Hamburg and their instructors \n\n " +
                "Assets from: \n OpenGameArt.org  " ;
        }

        //LOAD ALL SPECIFIC ITEMS FOR CREATION OF THE CREDITS MENU

        public override void LoadContent()
        {
            SpriteFont font = GameManager.LoadSpriteFont( "Aon Cari Celtic" );
            backgroundMusic = GameManager.LoadSoundEffect( "backgroundSound" ).CreateInstance();
            backgroundMusic.IsLooped = true;
            backgroundMusic.Play();

            background = new UIImage();
            background.Image = GameManager.Content.Load<Texture2D>( "landscape" );
            background.Position = new Vector2( 0, 0 );

            titel = new UILabel();
            titel.Font = font;
            titel.Position = new Vector2( 600, 0 );
            titel.Scale = 2f;

            text = new UILabel();
            text.Font = font;
            text.Position = new Vector2( 600, 45 );
            text.Scale = 1f;
            text.Color = Color.Black;

            creditsInfo = new UILabel();
            creditsInfo.Font = font;
            creditsInfo.Position = new Vector2( 600, 75 );
            creditsInfo.Scale = 1f;
            creditsInfo.Color = Color.Black;


            nextButtonExit = new UIButton( GameManager.Content.Load<Texture2D>( "menuButton" ) );
            nextButtonExit.Position = new Vector2( 425, 365 );
            nextButtonExit.OnPress += BackToMainMenu;

            buttonLabelExit = new UILabel();
            buttonLabelExit.Font = font;
            buttonLabelExit.Position = new Vector2( 665, 360 );
            buttonLabelExit.Scale = 1.5f;
            buttonLabelExit.Color = Color.White;
            buttonLabelExit.Text = "Back";
        }

        public override void UnloadContent()
        {
            backgroundMusic.Stop();
            background.UnloadContent();
            titel.UnloadContent();
            text.UnloadContent();
            nextButtonExit.UnloadContent();
            buttonLabelExit.UnloadContent();
        }

        public override void Update( GameTime gameTime )
        {
        }

        public override void Draw( SpriteBatch spriteBatch )
        {
        }
    }
}