﻿
//AUTHOR Christopher Grimm
//LAST EDITED: 28.02.2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Dont_Give_Up
{
    //IMAGE ELEMENT
    class UIImage : UIElement
    {
        public Texture2D Image;
        public Vector2 Position;

        //DRAW IMAGE ELEMENT
        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Image, Position, Color.White);
        }
    }
}
