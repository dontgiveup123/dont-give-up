﻿//AUTHOR Christopher Grimm
//LAST EDITED: 28.02.2016

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace Dont_Give_Up
{
    class UIManager
    {
        private static List<UIElement> uiElements = new List<UIElement>();

        //ADD ELEMENT TO UIMANAGER
        public static void AddElement(UIElement element)
        {
            uiElements.Add(element);
        }
        
        //UPDATE UIELEMENTS IN LIST
        public static void Update(GameTime gameTime)
        {
            uiElements.ForEach(e => e.Update(gameTime));
        }
        
        //DRAW ELEMENTS IN LIST
        public static void Draw(SpriteBatch spriteBatch)
        {
            uiElements.ForEach(e => e.Draw(spriteBatch));
        }
        
        //REMOVE ALL ELEMENTS IN LIST
        public static void Clear()
        {
            uiElements.Clear();
        }
        
        //REMVOE SPECIFIC ELEMENT FROM LIST
        public static void RemoveElement(UIElement element)
        {
            uiElements.Remove( element );
        }
    }
}
