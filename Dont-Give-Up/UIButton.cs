﻿//AUTHOR Christopher Grimm
//LAST EDITED: 28.02.2016

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dont_Give_Up
{
    //BUTTON ELEMENT
    class UIButton : UIElement
    {
        public delegate void ButtonEvent();
        public event ButtonEvent OnPress;
        private Rectangle bounds;

        public Texture2D Image { get; private set; }

        public Vector2 Position
        {
            get { return new Vector2(bounds.X, bounds.Y); }
            set { bounds.X = (int)value.X; bounds.Y = (int)value.Y; }
        }

        public UIButton(Texture2D image)
        {
            Image = image;
            bounds = image.Bounds;
        }
        
        //CONTINOUSLY CHECK IF MOUSE IS CLICKED
        public override void Update(GameTime gameTime)
        {
            CheckMouseClick();
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Image, bounds, Color.White);
        }
        
        //CHECK MOUSE CLICKS AND BOUNDS OF IMAGE THEN EXECUTE ONPRESS
        private void CheckMouseClick()
        {
            MouseState mouseState = Mouse.GetState();
            if (mouseState.LeftButton == ButtonState.Pressed)
            {
                if (bounds.Contains(mouseState.Position))
                {
                    if (OnPress != null)
                        OnPress();
                }
            }
        }

        public override void UnloadContent()
        {
            base.UnloadContent();
            OnPress = null;
        }
    }
}
