﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace Dont_Give_Up
{
    //Author: Henrik Winter

    //one tile objekte is one filed on the map.
    class Tile
    {
        public Texture2D sprite;
        public bool IsPassable;
        public bool Degradable;

        public Tile(Texture2D sprite, bool isPassable, bool degradable)
        {
            this.sprite = sprite;
            this.IsPassable = isPassable;
            this.Degradable = degradable;
        }
    }
}
