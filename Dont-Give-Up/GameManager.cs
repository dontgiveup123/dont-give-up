﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace Dont_Give_Up
{
    //Author: Henrik Winter

    //the GameManager class load the Content folder and manage the Gameobjekts shown on the map.
    class GameManager
    {
        public static ContentManager Content;
        public static SpriteBatch SpriteBatch;
        public static SpriteFont SpriteFont;

        private static int currentMapNumber;
        private static List<Map> maps = new List<Map>();
        private static List<GameObject> gameObjects = new List<GameObject>();


        public static void AddGameObject( GameObject gameObject )
        {
            gameObjects.Add( gameObject );
        }

        public static void AddMap(Map map)
        {
            maps.Add(map);
        }

        public static Texture2D LoadTexture2D( string name )
        {
            return Content.Load<Texture2D>( name );
        }

        public static SoundEffect LoadSoundEffect(string name )
        {
            return Content.Load<SoundEffect>( name );
        }

        public static SpriteFont LoadSpriteFont(string name )
        {
            return Content.Load<SpriteFont>( name );
        }

        public static void Update( GameTime gameTime )
        {
            foreach( var gameObject in gameObjects )
                gameObject.Update( gameTime );
        }

        public static void Draw(SpriteBatch spriteBatch)
        {
            SpriteBatch.Begin();

            foreach( var gameObject in gameObjects )
                gameObject.Draw( SpriteBatch );

            SpriteBatch.End();
        }

        public static GameObject FindGameObject(string name)
        {
            return gameObjects.Find(g => g.Name == name);
        }

        public static void LoadMap(int newMapNumber)
        {
            currentMapNumber += newMapNumber;
            gameObjects.RemoveAll(x => x.Name == "Map");
            gameObjects.RemoveAll(x => x.Name == "Player");
            var newMap = maps.Find(x => x.MapNumber == currentMapNumber);
            if (newMap==null)
            {
                AddMap(new Map(currentMapNumber));
                newMap = maps.Find(x => x.MapNumber == currentMapNumber);
            }
            AddGameObject(newMap);
        }

        public static void UnloadMap()
        {
            Map map;
            maps.RemoveAll(x => x.MapNumber == currentMapNumber);
            var obj = gameObjects.Find((x => x.Name == "Map"));
            map = (Map)obj;
            AddMap(map);
        }
    }
}
