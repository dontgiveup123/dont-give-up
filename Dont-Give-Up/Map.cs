﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Dont_Give_Up
{
    //Author: Henrik Winter

    //the Map class create a randome Map and Update it when somthing is changed.
    class Map : GameObject
    {
        public const int Width = 36;
        public const int Height = 24;
        public int MapNumber;

        private const int treeProcent = 50;
        private const int boarderMin = 12;
        private const int boarderMax = 13;
        private const int stones = 20;

        private Tile[,] tiles = new Tile[Width, Height];

        private Texture2D grass;
        private Texture2D tree;
        private Texture2D stoneWall;
        private Texture2D woodWall;
        private Texture2D rock;
        private Texture2D levelWall;
        private Tile grassTile;
        private Tile treeTile;
        private Tile stoneWallTile;
        private Tile woodWallTile;
        private Tile rockTile;
        private Tile levelWallTile;

        public Map(string name)
        {
            this.Name = name;
            
        }

        public Map(int mapNumber)
        {
            this.Name = "Map";
            this.MapNumber = mapNumber;
            
            LoadTextures();
            Init();
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            for (int y = 0; y < Height; y++)
            {
                for (int x = 0; x < Width; x++)
                {
                    spriteBatch.Draw(tiles[x, y].sprite, new Vector2(x * grass.Width, y * grass.Height), Color.AliceBlue);
                }
            }
        }

        public Tile GetTile(Vector2 position)
        {
            return tiles[(int)position.X, (int)position.Y];
        }

        public Tile GetTile(int x, int y)
        {
            return tiles[x, y];
        }

        public void Degrad(int x, int y)
        {
            if(tiles[x, y] == treeTile)
            {
                Inventory.AddItem(ItemTypes.wood, 1);
            }
            else if(tiles[x, y] == rockTile)
            {
                Inventory.AddItem(ItemTypes.stone, 3);
            }
            else if (tiles[x, y] == stoneWallTile)
            {
                Inventory.AddItem(ItemTypes.stone, 1);
            }

            tiles[x, y] = grassTile;
        }

        public void PlaceWall(int x, int y)
        {
            if(tiles[x, y] == grassTile)
            {
                if (Player.BuildMaterial == true)
                    if (Inventory.RemoveItem(ItemTypes.wood, 1))
                        tiles[x, y] = woodWallTile;
                if (Player.BuildMaterial == false)
                    if (Inventory.RemoveItem(ItemTypes.stone, 1))
                        tiles[x, y] = stoneWallTile;
            }
        }

        private void LoadTextures()
        {
            grass = GameManager.LoadTexture2D("grass");
            tree = GameManager.LoadTexture2D("tree");
            stoneWall = GameManager.LoadTexture2D("stoneWall");
            woodWall = GameManager.LoadTexture2D("woodWall");
            rock = GameManager.LoadTexture2D("rock");
            levelWall = GameManager.LoadTexture2D("levelWall");
            grassTile = new Tile(grass, true, false);
            treeTile = new Tile(tree, false, true);
            stoneWallTile = new Tile(stoneWall, false, true);
            woodWallTile = new Tile(woodWall, false, true);
            rockTile = new Tile(rock, false, true);
            levelWallTile = new Tile(levelWall, false, false);
        }

        //Map creating start
        private void Init()
        {
            FirstForestVersion();
            ForestBoarder();
            for (int i = 0; i < 3; i++)
                CountTreesAround();
            SetRocks();
        }

        private void FirstForestVersion()
        {
            int randomInt;
            Random random = new Random();
            for (int x = 0; x < Width; x++)
                for (int y = 0; y < Height; y++)
                {
                    if (x == 0 || x == Width - 1 || y == 0 || y == Height - 1)
                        tiles[x, y] = levelWallTile;
                    else
                    {
                        randomInt = random.Next(1, 100);
                        SetTrees(x, y, randomInt);
                    }
                }
        }

        private void SetTrees(int x, int y, int randomInt)
        {
            if (randomInt <= treeProcent)
                tiles[x, y] = treeTile;
            else
                tiles[x, y] = grassTile;
        }

        private void ForestBoarder()
        {
            for(int y = boarderMin; y <= boarderMax; y++)
                for (int x = 0; x < Width; x++)
                    tiles[x, y] = grassTile;
        }

        private void CountTreesAround()
        {
            for (int x = 0; x < Width; x++)
                for (int y = 0; y < Height; y++)
                {
                    int treeCount = 0;
                    treeCount = CheckSurroundings(x, y, treeCount);
                    GladeMaker(x, y, treeCount);
                }
        }

        private int CheckSurroundings(int x, int y, int treeCount)
        {
            if (CheckOneField(x - 1, y - 1) == true)
                treeCount += 1;
            if (CheckOneField(x, y - 1) == true)
                treeCount += 1;
            if (CheckOneField(x + 1, y - 1) == true)
                treeCount += 1;
            if (CheckOneField(x - 1, y) == true)
                treeCount += 1;
            if (CheckOneField(x + 1, y) == true)
                treeCount += 1;
            if (CheckOneField(x - 1, y + 1) == true)
                treeCount += 1;
            if (CheckOneField(x, y + 1) == true)
                treeCount += 1;
            if (CheckOneField(x + 1, y + 1) == true)
                treeCount += 1;
            return treeCount;
        }

        private bool CheckOneField(int x, int y)
        {
            bool Count;
            if (x >= Width || x < 0 || y >= Height || y < 0)
                Count = false;
            else if (tiles[x, y] == treeTile || tiles[x, y] == levelWallTile)
                Count = true;
            else
                Count = false;
            return Count;
        }

        private void GladeMaker(int x, int y, int treeCount)
        {
            if (tiles[x, y] == treeTile)
            {
                if (treeCount < 4)
                    tiles[x, y] = grassTile;
            }

            else if (tiles[x, y] == grassTile)
            {
                if (treeCount >= 5)
                    tiles[x, y] = treeTile;
            }
        }
        
        private void SetRocks()
        {
            int randomeX;
            int randomeY;
            Random random = new Random();
            for (int i = 0; i < stones; i++)
            {
                randomeX = random.Next(2, Width - 1);
                randomeY = random.Next(2, Height - 1);
                tiles[randomeX, randomeY] = rockTile;
            }
        }
        //Map creating end
    }
}   
