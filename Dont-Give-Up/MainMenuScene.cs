﻿//AUTHOR Christopher Grimm
//LAST EDITED: 28.02.2016


using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Dont_Give_Up
{
    class MainMenuScene : Scene
    {
        private Texture2D background;
        private UIButton startButton;

        private MainMenuDialog mainQuestDialog;

        public override void LoadContent()
        {
            background = GameManager.Content.Load<Texture2D>( "landscape" );
            mainQuestDialog = new MainMenuDialog();
            mainQuestDialog.Titel = "Dont Give UP";
            mainQuestDialog.Text = "Welcome Stranger to an unknown world in which you have to survive mother nature!";
            mainQuestDialog.Show();
        }

        public override void Update(GameTime gameTime)
        {
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(background, background.Bounds, Color.White);
        }

        public override void UnloadContent()
        {
            mainQuestDialog.UnloadContent();
        }
    }
}
