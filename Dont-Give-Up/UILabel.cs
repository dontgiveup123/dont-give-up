﻿//AUTHOR Christopher Grimm
//LAST EDITED: 28.02.2016

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Dont_Give_Up
{
    //TEXT ELEMENT
    public class UILabel : UIElement
    {
        public SpriteFont Font;
        public Vector2 Position;
        public string Text;
        public float Scale;
        public Color Color = Color.Black;

        //DRAW TEXT ELEMENT
        public override void Draw(SpriteBatch spriteBatch)
        {
           var unitsSubtracted = Font.MeasureString( Text ) / 2 * Vector2.UnitX;
            spriteBatch.DrawString(Font, Text, Position - unitsSubtracted, Color, 0f, Vector2.Zero, Scale, SpriteEffects.None, 0);
        }
    }
}
