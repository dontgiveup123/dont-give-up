﻿//AUTHOR Christopher Grimm
//LAST EDITED: 28.02.2016

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dont_Give_Up
{
    public abstract class UIElement
    {
        public UIElement()
        {
            UIManager.AddElement(this);
        }

        public virtual void Update(GameTime gameTime)
        {
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
        }

        public virtual void UnloadContent()
        {
            UIManager.RemoveElement( this );
        }
    }
}
